package ru.t1.dkozoriz.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.api.repository.IAbstractRepository;
import ru.t1.dkozoriz.tm.model.AbstractModel;

import java.util.*;

public abstract class AbstractRepository<T extends AbstractModel> implements IAbstractRepository<T> {

    @NotNull
    private final Map<String, T> models = new LinkedHashMap<>();

    @Override
    @NotNull
    public T add(@NotNull final T model) {
        models.put(model.getId(), model);
        return model;
    }

    public void addAll(@NotNull final List<T> model) {
        models.values().addAll(model);
    }

    @Override
    public void clear() {
        models.clear();
    }

    @Override
    @NotNull
    public List<T> findAll() {
        return new ArrayList<>(models.values());
    }

    @Nullable
    public T findById(@NotNull final String id) {
        return models.get(id);
    }

    @Nullable
    public T findByIndex(@NotNull final Integer index) {
        return models.get(index);
    }

    public int getSize() {
        return models.size();
    }

    @Nullable
    public T remove(@Nullable final T model) {
        if (model == null) return null;
        models.remove(model.getId());
        return model;
    }

    @Nullable
    public T removeById(@NotNull final String id) {
        @Nullable final T model = findById(id);
        return remove(model);
    }

    @Nullable
    public T removeByIndex(@NotNull final Integer index) {
        @Nullable final T model = findByIndex(index);
        return remove(model);
    }

    public void removeAll(@NotNull final Collection<T> collection) {
        collection.stream()
                .map(AbstractModel::getId)
                .forEach(models::remove);
    }

}