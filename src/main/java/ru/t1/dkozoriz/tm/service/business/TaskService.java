package ru.t1.dkozoriz.tm.service.business;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.api.repository.business.ITaskRepository;
import ru.t1.dkozoriz.tm.api.service.business.ITaskService;
import ru.t1.dkozoriz.tm.exception.field.DescriptionEmptyException;
import ru.t1.dkozoriz.tm.exception.field.NameEmptyException;
import ru.t1.dkozoriz.tm.exception.user.UserIdEmptyException;
import ru.t1.dkozoriz.tm.model.business.Task;

import java.util.Collections;
import java.util.List;

public final class TaskService extends BusinessService<Task, ITaskRepository> implements ITaskService {

    private final static String NAME = "Task";

    @NotNull
    @Override
    protected String getName() {
        return NAME;
    }

    public TaskService(@NotNull final ITaskRepository taskRepository) {
        super(taskRepository);
    }

    @NotNull
    @Override
    public Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        return add(userId, task);
    }

    @NotNull
    public Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description,
            @Nullable final String projectId
    ) {
        @NotNull Task task = create(userId, name, description);
        task.setProjectId(projectId);
        return add(userId, task);
    }

    @NotNull
    public List<Task> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return repository.findAllByProjectId(userId, projectId);
    }

}