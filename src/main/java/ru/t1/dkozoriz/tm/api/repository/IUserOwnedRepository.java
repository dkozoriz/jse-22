package ru.t1.dkozoriz.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.model.UserOwnedModel;

import java.util.List;

public interface IUserOwnedRepository<T extends UserOwnedModel> extends IAbstractRepository<T>{

    void clear(@Nullable String userId);

    boolean existById(@Nullable String userId, @Nullable String id);

    @NotNull
    List<T> findAll(@Nullable String userId);

    @Nullable
    T findById(@Nullable String userId, @Nullable String id);

    @Nullable
    T findByIndex(@Nullable String userId, @NotNull Integer index);

    int getSize(@Nullable String userId);

    @Nullable
    T removeById(@Nullable String userId, @Nullable String id);

    @Nullable
    T removeByIndex(@Nullable String userId, @NotNull Integer index);

    @Nullable
    T add(@Nullable String userId, @NotNull T model);

    @Nullable
    T remove(@Nullable String userId, @Nullable T model);

}
