package ru.t1.dkozoriz.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.model.AbstractModel;

import java.util.List;
import java.util.Optional;

public interface IAbstractRepository<T> {

    interface IRepositoryOptional<T> {

        Optional<T> findById(String id);

        Optional<T> findByIndex(Integer index);

    }

    default IRepositoryOptional<T> optional() {
        final IAbstractRepository<T> repository = this;
        return new IRepositoryOptional<T>() {
            @Override
            public Optional<T> findById(final String id) {
                return Optional.ofNullable(IAbstractRepository.this.findById(id));
            }
            @Override
            public Optional<T> findByIndex(final Integer index) {
                return Optional.ofNullable(IAbstractRepository.this.findByIndex(index));
            }
        };
    }

    @NotNull
    List<T> findAll();

    @NotNull
    T add(@NotNull T project);

    void clear();

    @Nullable
    T findById(@NotNull String id);

    int getSize();

    @Nullable
    T remove(@Nullable T project);

    @Nullable
    T removeById(@NotNull String id);

    @Nullable
    T findByIndex(@NotNull Integer index);

    @Nullable
    T removeByIndex(@NotNull Integer index);

}